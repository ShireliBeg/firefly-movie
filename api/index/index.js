import request from "../request"

function  in_theaters(data) {
  return request({
    url:"/api/douban/movie/in_theaters",
    data
  })
}
function  coming_soon(data) {
  return request({
    url:"/api/douban/movie/coming_soon",
    data
  })
}
function  selectPreview(data) {
  return request({
    url:"/api/douban/movie/selectPreview",
    data
  })
}

export {
  in_theaters,
  coming_soon,
  selectPreview
}