import baseURL from "./baseUrl"

var fun = function(config){
  return new Promise((resolve,rejected)=>{
    wx.request({
      url:baseURL + config.url,
      timeout:5000,
      data:config.data,
      method:config.method,
      success(res){
        resolve(res.data)
      }
    })
  })
}


export default fun;