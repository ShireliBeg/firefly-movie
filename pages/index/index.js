// index.js
// 获取应用实例
const app = getApp()
import QQMapWX  from "../../utils/qqmap-wx-jssdk1.2/qqmap-wx-jssdk";
var qqmapsdk;
import {
  in_theaters,
  coming_soon,
  selectPreview
} from "../../api/index/index";
Page({
  data: {
    currentTabIndex:0,
    isAddClass:false,
    location:"",
    address:'',//cinema页面展示的定位具体位置
    locationObj:{},// 定位的经纬度对象 传递到cinema页面
    tabList:[
      {
        icon:"/static/icons/home.png",
        selectedIcon:"/static/icons/home-active.png",
        path:"/pages/index/index?index=0"
      },{
        icon:"/static/icons/movie.png",
        selectedIcon:"/static/icons/movie-active.png",
        path:"/pages/index/index?index=1"
      }
      // ,{
      //   icon:"/static/icons/ticket-active.png",
      //   selectedIcon:"/static/icons/ticket-active.png",
      //   path:"/pages/index/index?index=2"
      // }
      ,{
        icon:"/static/icons/cinema.png",
        selectedIcon:"/static/icons/cinema-active.png",
        path:"/pages/index/index?index=3"
      },{
        icon:"/static/icons/main.png",
        selectedIcon:"/static/icons/main-active.png",
        path:"/pages/index/index?index=4"
      },
    ],
    in_theaters:[],
    coming_soon:[],
    selectPreview:[],
  },
  goTab(e){
    var index = e.currentTarget.dataset.index;
    
    this.setData({
      currentTabIndex:index,
      isAddClass:false
    })
    if(index == 2){
      this.setData({
        isAddClass:true
      })
    }
  },
  onLoad(options){
    var _this = this;
    this.setData({
      currentTabIndex:options && options.id
    })
    // 定位
    qqmapsdk = new QQMapWX({
      key: 'Y25BZ-PAG3I-R66G3-56L24-EU6JV-ZWBQU'
    });
    wx.getLocation({
      success(res){
        qqmapsdk.reverseGeocoder({
          location:{
            latitude: res.latitude,
            longitude: res.longitude
          },
          coord_type:1,
          success(res1){
            // console.log("res1==>",res1);
            _this.setData({
              location:res1.result.address_component.city,
              address:res1.result.address,
              locationObj:res
            })
          }
        })
      }
    })
    // home页面 三个请求
    in_theaters()
    .then(res =>{
      
      this.setData({
        in_theaters:res.data.list
      })
    })

    coming_soon()
    .then(res =>{
      
      this.setData({
        coming_soon:res.data.list
      })
    })

    selectPreview()
    .then(res =>{
      
      this.setData({
        selectPreview:res.data
      })
    })
  },
  // 电影页面选择城市后，修改location 的 自定义事件
  changeLocation(e){
    // console.log("dddd=>",e)
    this.setData({
      location:e.detail.name
    })
  }
})
