// pages/index/components/movie/movie.js
// 获取城市选择插件
const city_choice = requirePlugin('city-choice');
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    in_theaters: Array,
    coming_soon: Array,
    selectPreview: Array,
    location: String
  },

  /**
   * 组件的初始数据
   */
  data: {
    // 自定义热门城市
    hotCitys: '珠海,深圳,汕头,惠州,梅州,广州',
    // 当前tab状态
    currentTabStatus:0, // 0 正在热映， 1 即将上映,
    comingDate:[], // 即将上映日期数组
    current:0,// 当前选中的 即将上映日期 的下标
  },
  lifetimes: {
    created() {
      wx.getSystemInfo({
        success: res => {
          // console.log(res);
          this.setData({
            // isDark: res.theme == 'dark' ? true : false //跟随系统是否开启深色模式
            isDark:true
          })
        }
      })
    },
    attached(){
      // 初始化当前月一共几天
      this.getComingDate()
    }
    
  },
  pageLifetimes:{
    show:function(){
     
      let getcity = city_choice.getcity() //返回选择的城市信息，未选择则返回null
      // 触发自定义事件
      this.triggerEvent('changecityevent',getcity)
    }
  },


  /**
   * 组件的方法列表
   */
  methods: {
    // 跳转到城市选择页面
    goChooseCity() {
      wx.navigateTo({
        url: `plugin://city-choice/city-choice?onHistory=${true}&onDarkMode=${this.data.isDark}&hotCitys=${this.data.hotCitys}`
      })
    },
    // 顶部tabbar 切换事件
    changeTabEvent(e) {
      console.log(e);
      let tab = e.currentTarget.dataset.tab
      this.setData({
        currentTabStatus:tab
      })
      if (tab == 0) { // 正在热映

      } else if (tab == 1) { // 即将上映

      }
    },
    // 获取即将上映日期
    getComingDate(){
      let date = new Date()
      let year = date.getFullYear();
      let month = date.getMonth()+1;
      let d = new Date(year, month, 0);
      let totalDay =  d.getDate();
      let dateArr = []
      for(let i = date.getDate();i<=totalDay;i++){
        dateArr.push(`${month}月${i}日`)
      }
      this.setData({
        comingDate:dateArr
      })
      // console.log(`当前月${month}:共${totalDay}天`)
    },
    // 点击日期触发
    changeDate(e){
      // console.log(e.currentTarget.dataset.idx);
      let idx = e.currentTarget.dataset.idx
      this.setData({
        current:idx
      })
    },
    // 跳转电影详情页面
    goMovieDetail(e){
      let id = e.currentTarget.dataset.id
      wx.navigateTo({
        url: `/pages/homeViews/info/info?id=${id}`,
      })
    }


  }
})