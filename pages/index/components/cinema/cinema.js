import fun from "../../../../api/request";
import QQMapWX  from "../../../../utils/qqmap-wx-jssdk1.2/qqmap-wx-jssdk.min";
var qqmapsdk;
// 获取城市选择插件
const city_choice = requirePlugin('city-choice');
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    location: String,
    address:String,
    locationObj:Object,// 首页定位的经纬度对象
  },
  observers:{
    'locationObj':function(val){
      // console.log("val==>",val.latitude);
      // 将定位信息保存在本地
      wx.setStorageSync('location', val)
      if(val){
        // 地点搜索
        var _this = this;
        qqmapsdk.search({
          keyword:this.data.keyWords,
          location:{
            latitude:val.latitude,
            longitude: val.longitude
          },
          success(res){
            console.log("res=>",res);
            _this.setData({
              cinemaList:res.data
            })
            // 将影院信息保存本地，方便影院页面使用
            wx.setStorageSync('cinemaList', res.data)
          },
          fail(err){
            console.log("err=>",err);
          }
        })
      }
    }
  },
  lifetimes:{
    created(){
      wx.getSystemInfo({
        success: res => {
          // console.log(res);
          this.setData({
            // isDark: res.theme == 'dark' ? true : false //跟随系统是否开启深色模式
            isDark:true
          })
        }
      })

      // console.log(this);
      qqmapsdk = new QQMapWX({
        key: 'Y25BZ-PAG3I-R66G3-56L24-EU6JV-ZWBQU'
      });
      
    },
    attached(){
     
     
    }
   
  },

  /**
   * 组件的初始数据
   */
  data: {
     // 自定义热门城市
     hotCitys: '珠海,深圳,汕头,惠州,梅州,广州',
    keyWords:'影院',
    cinemaList:[]
  },
  pageLifetimes:{
    show:function(){
     
      let getcity = city_choice.getcity() //返回选择的城市信息，未选择则返回null
      // 触发自定义事件
      this.triggerEvent('changecityevent',getcity)
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 跳转到城市选择页面
    goChooseCity() {
      wx.navigateTo({
        url: `plugin://city-choice/city-choice?onHistory=${true}&onDarkMode=${this.data.isDark}&hotCitys=${this.data.hotCitys}`
      })
    },
    // 跳转到影院地图页面
    goCinamaMapPage(){
      console.log(1111);
      wx.navigateTo({
        url: '/pages/cinemaViews/map/map',
      })
    },
    // 跳转到到影院详情页面
    goCinemaDetail(e){
      let id = e.currentTarget.dataset.id
      wx.navigateTo({
        url: `/pages/cinemaViews/detail/detail?id=${id}`,
      })
    }
  }
})
