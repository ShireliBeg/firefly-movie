// pages/index/components/home/home.js
const city_choice = requirePlugin('city-choice');
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    in_theaters:Array,
    coming_soon:Array,
    selectPreview:Array,
    location:String
  },

  /**
   * 组件的初始数据
   */
  data: {
    current:0
  },
  pageLifetimes:{
    show:function(){
     
      let getcity = city_choice.getcity() //返回选择的城市信息，未选择则返回null
      // 触发自定义事件
      this.triggerEvent('changecityevent',getcity)
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    swiperChange(e){
      this.setData({
        current:e.detail.current
      })
    },
    chooseLocation(){
      wx.navigateTo({
        url: `plugin://city-choice/city-choice?onHistory=${true}&onDarkMode=${false}&hotCitys=`
      })
    },
    goInfo(e){
      wx.navigateTo({
        url: `/pages/homeViews/info/info?id=${e.currentTarget.dataset.id}`,
      })
    }
  }
})
