// pages/index/components/user/user.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    orderTab:[
      {
        icon:"/static/images/user/wxf.png",
        id:0,
        name:"全部"
      },
      {
        icon:"/static/images/user/dfk.png",
        id:1,
        name:"待付款"
      },
      {
        icon:"/static/images/user/dpj.png",
        id:2,
        name:"待评价"
      },
      {
        icon:"/static/images/user/tk.png",
        id:3,
        name:"已付款"
      },
    ],
    otherList:[
      {
        name:"影城会员卡",
        // path:"/pages/userViews/card/card"
        path:""
      },
      {
        name:"优惠券",
        path:"/pages/userViews/coupon/coupon"
      },
      {
        name:"我的收藏",
        path:""
      },
      {
        name:"帮助与反馈",
        path:""
      },
    ]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    goUserInfo(){
      wx.navigateTo({
        url: '/pages/userViews/userInfo/userInfo',
      })
    },
    goOrder(e){
      var id = e.currentTarget.dataset.id ? e.currentTarget.dataset.id : 0;
      wx.navigateTo({
        url: `/pages/userViews/order/order?id=${id}`,
      })
    },
    goOthers(e){
      var path = e.currentTarget.dataset.path;
      wx.navigateTo({
        url: path,
      })
    }
  }
})
