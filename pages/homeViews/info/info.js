// pages/homeViews/info/info.js
import {
  subject
} from "../../../api/homeViews/info/index"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:{},
    rating_stars_integer:0,
    rating_stars_decimal:0,
    tabList:["简介","影评","讨论","更多"],
    currentTabIndex:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
   var res = await subject({
      mId:options.id
    })
    console.log("元数据",res.data.rating_average,parseInt(res.data.rating_average),res.data.rating_average.split("."))
    this.setData({
      info:res.data,
      rating_stars_integer:parseInt(parseInt(res.data.rating_average)/2),
      rating_stars_decimal:res.data.rating_average.split(".")[1]
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})