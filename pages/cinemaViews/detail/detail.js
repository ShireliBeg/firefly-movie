// pages/cinemaViews/detail/detail.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        current:0,// 当前轮播图位置
        cinemaId:'' ,// 影院ID
        cinemaObj:{},//影院对象
        bannerList:[
            {
                id:0,
                name:'后来的我们',
                imgURL:'https://zhujinlong.gitee.io/leju-admin-2110/static/img/1.png',
                director:['张艺谋','罗贯中'],
                genres:['剧情'],
                scriptwriter:['周冬雨'],
                longtime:'119分钟',
                sort:[
                    {
                        id:1,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'2号厅(冠名招商中)',
                        price:'30.9'
                    },
                    {
                        id:2,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'2号厅(冠名招商中)',
                        price:'30.9'
                    },
                    {
                        id:3,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'2号厅(冠名招商中)',
                        price:'30.9'
                    },
                    {
                        id:4,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'2号厅(冠名招商中)',
                        price:'30.9'
                    },
                    {
                        id:5,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'2号厅(冠名招商中)',
                        price:'30.9'
                    }
                ]
            },
            
            {
                id:1,
                name:'傲慢与偏见',
                imgURL:'https://zhujinlong.gitee.io/leju-admin-2110/static/img/2.png',
                director:['张艺谋','罗贯中'],
                genres:['剧情'],
                scriptwriter:['周冬雨'],
                longtime:'119分钟',
                sort:[
                    {
                        id:1,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'3号厅(冠名招商中)',
                        price:'30.9'
                    },
                    {
                        id:2,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'3号厅(冠名招商中)',
                        price:'30.9'
                    },
                    {
                        id:3,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'3号厅(冠名招商中)',
                        price:'30.9'
                    },
                    {
                        id:4,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'3号厅(冠名招商中)',
                        price:'30.9'
                    },
                    {
                        id:5,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'3号厅(冠名招商中)',
                        price:'30.9'
                    }
                ]
            },
            {
                id:2,
                name:'满城尽带黄金甲',
                imgURL:'https://zhujinlong.gitee.io/leju-admin-2110/static/img/3.png',
                director:['张艺谋','罗贯中'],
                genres:['剧情'],
                scriptwriter:['周冬雨'],
                longtime:'119分钟',
                sort:[]
            },
            {
                id:3,
                name:'战狼2',
                imgURL:'https://zhujinlong.gitee.io/leju-admin-2110/static/img/4.png',
                director:['张艺谋','罗贯中'],
                genres:['剧情'],
                scriptwriter:['周冬雨'],
                longtime:'119分钟',
                sort:[]
            },
            {
                id:4,
                name:'让子弹飞一会儿',
                imgURL:'https://zhujinlong.gitee.io/leju-admin-2110/static/img/5.png',
                director:['张艺谋','罗贯中'],
                genres:['剧情'],
                scriptwriter:['周冬雨'],
                longtime:'119分钟',
                sort:[
                    {
                        id:1,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'8号厅(冠名招商中)',
                        price:'40.9'
                    },
                    {
                        id:2,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'8号厅(冠名招商中)',
                        price:'40.9'
                    },
                    {
                        id:3,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'8号厅(冠名招商中)',
                        price:'40.9'
                    },
                    {
                        id:4,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'8号厅(冠名招商中)',
                        price:'40.9'
                    },
                    {
                        id:5,
                        start:"15:30",
                        end:'17:14',
                        lx:'原版3D',
                        location:'8号厅(冠名招商中)',
                        price:'40.9'
                    }
                ]
            }

        ],
        today:'',//今日文案 2月1号
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            cinemaId:options.id
        })
    },
    onShow(){
        let cinemaList = wx.getStorageSync('cinemaList')
        if(cinemaList){
            let item = cinemaList.find(ele=>ele.id == this.data.cinemaId)
            this.setData({
                cinemaObj:item
            })
        }
        this.getDate()
    },
    swiperChange(e){
       this.setData({
        current:e.detail.current
       })
    },
    getDate(){
        let date = new Date()
        let month = date.getMonth()+1
        let day = date.getDate()
        this.setData({
            today:month+"月"+day+"日"
        })
    },
    // 跳转到选座页面
    goChooseSeat(){
        let movieName = this.data.bannerList[this.data.current].name
        wx.navigateTo({
          url: `/pages/cinemaViews/seat/seat?movieName=${movieName}`,
        })
    },
    goBack(){
        wx.navigateBack({
          delta: 1,
        })
    }

   
})