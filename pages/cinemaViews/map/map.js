// pages/cinemaViews/map/map.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        location:{},// 定位信息
        markers:[],// 地图上的marker
        cinemaList:[],// 影院信息
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.setNavigationBarTitle({
          title: '影院地图',
        })
    },

    onShow(){
        let location = wx.getStorageSync('location')
        let cinemaList = wx.getStorageSync('cinemaList')
        this.setData({
            location,
            cinemaList
        })
        this.buildMarkers()

    },
    buildMarkers(){
        let tempMarkers = []
        this.data.cinemaList.forEach((ele,index)=>{
            let marker = {
                id:Number(ele.id),
                latitude:ele.location.lat,
                longitude:ele.location.lng,
                title:ele.title,
                width:'44rpx',
                height:'60rpx',
                iconPath:'/static/images/cinema/marker.png',
                label:{
                    content:index+1,
                    color:'#ffffff',
                    fontSize:'36rpx',
                    textAlign:'center',
                    anchorX:0,
                    anchorY:-30
                }

            }
            tempMarkers.push(marker)
        })

        this.setData({
            markers:tempMarkers
        })
    },
    // 拨打电话
    callCinema(e){
        let tel = e.currentTarget.dataset.tel
        wx.makePhoneCall({
          phoneNumber: tel
        })
    }
})