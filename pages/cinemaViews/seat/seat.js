// pages/cinemaViews/seat/seat.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        movieName:'',
        seatList:[
           0,0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,
           0,2,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,1,1,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,2,0,
           0,0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0
        ], // 0 可选 1 不可选 2 已选
        rowNum:9,// 9行
        colNum:12,// 12列
        recommendIdx:null,// 推荐座位的下标
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            movieName:options.movieName
        })
    },
    recommendSeat(e){
        let idx = e.currentTarget.dataset.idx
        this.setData({
            recommendIdx:idx
        })
        let count = 0;
        // let canSelectedList = this.data
        while(true){
            let randomNum = Math.floor(Math.random()*(this.data.seatList.length-6))
            if(this.data.seatList[randomNum] !=1 && this.data.seatList[randomNum] !=2){
                count++
                if(count>idx+1){
                    break;
                }
                this.setData({
                    [`seatList[${randomNum}]`]:2
                })
            }
        }

    },
    handleClickEvent(e){
        let idx = e.currentTarget.dataset.idx
        // var index = e.currentTarget.dataset.index;
        var val = `seatList[${idx}]`
        this.setData({
          [val]:2
        })
    },
    goOrder(){
        wx.navigateTo({
          url: '/pages/cinemaViews/order/order',
        })
    },
    goBack(){
        wx.navigateBack({
          delta: 1,
        })
    }
  
 

   
})