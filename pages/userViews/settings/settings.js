// pages/userViews/settings/settings.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[
      {
        name:"被赞提醒",
        isAddClass:false
      },
      {
        name:"影评/话题提醒",
        isAddClass:false
      },
      {
        name:"系统通知",
        isAddClass:false
      }
    ]
  },
  loginOut(){
    wx.clearStorageSync();
    wx.navigateTo({
      url: '/pages/userViews/login/login',
    })
  },
  toggle(e){
    var name = e.currentTarget.dataset.name;
    var item = this.data.list.find(ele =>ele.name == name);
    item.isAddClass = !item.isAddClass;
    this.setData({
      list:this.data.list
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})