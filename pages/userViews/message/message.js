// pages/userViews/message/message.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    noticeList:[
      {
        icon:"/static/images/user/notice.png",
        type:1,
        title:"系统通知",
        info:"哈皮巴斯得！祝你生日快乐！",
        time:"1个月前",
        color:"#f9c34a"
      },
      {
        icon:"/static/images/user/good.png",
        type:1,
        title:"赞",
        info:"一个赞都不给我！",
        time:"1个月前",
        color:"#ef8833"
      },
      {
        icon:"/static/images/user/message.png",
        type:1,
        title:"评论",
        info:"暂时还没有收到评论",
        time:"1个月前",
        color:"#6548a9"
      },
    ],
    messageBox:[
      {
        icon:"http://jinglins.gitee.io/movie/img/img1.9a33f779.png",
        type:0,
        title:"Zeng Wen",
        info:"暂时还没有收到评论",
        time:"1个月前",
        color:"#f9c34a"
      },
      {
        icon:"http://jinglins.gitee.io/movie/img/img2.552d2577.png",
        type:0,
        title:"Chikelu Obasea",
        info:"一个赞都不给我！",
        time:"1个月前",
        color:"#ef8833"
      },
      {
        icon:"http://jinglins.gitee.io/movie/img/img3.c9f2179e.png",
        type:0,
        title:"Kimmy McIlmorie",
        info:"暂时还没有收到评论",
        time:"1个月前",
        color:"#6548a9"
      },
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})