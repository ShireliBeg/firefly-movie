// pages/userViews/userInfo/userInfo.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[
      {
        starts:4,
        images:"http://jinglins.gitee.io/movie/img/movie1.c96045f9.png",
        title:"后来的我们",
        type:"动画/玄幻",
        area:"中国/105分钟",
        time:"购票时间 2016-8-4"
      },
      {
        starts:3,
        images:"http://jinglins.gitee.io/movie/img/movie2.b611b998.png",
        title:"帕丁顿熊",
        type:"动画/玄幻",
        area:"美国/105分钟",
        time:"购票时间 2017-8-4"
      },
      {
        starts:4,
        images:"http://jinglins.gitee.io/movie/img/movie1.c96045f9.png",
        title:"后来的我们",
        type:"动画/玄幻",
        area:"中国/105分钟",
        time:"购票时间 2016-8-4"
      },
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})