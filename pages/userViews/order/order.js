// pages/userViews/order/order.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabList:["全部","待付款","待评价","已退款",],
    currentTabIndex:0,
    orderList:[
      {
        image:"http://jinglins.gitee.io/movie/img/movie1.c96045f9.png",
        cinema:"万达国际影城",
        movieName:"最好的我们",
        price:"70元",
        time:"05月16日 17:00",
        seat:"五号厅 5排14座 5排15座 ",
        count:2,
        status:0
      },
      {
        image:"http://jinglins.gitee.io/movie/img/movie2.b611b998.png",
        cinema:"万达国际影城",
        movieName:"帕丁顿熊",
        price:"70元",
        time:"05月16日 17:00",
        seat:"五号厅 5排14座 5排15座 ",
        count:2,
        status:1
      },
      {
        image:"http://jinglins.gitee.io/movie/img/movie3.7bc4e9a2.png",
        cinema:"万达国际影城",
        movieName:"大鱼海棠",
        price:"80元",
        time:"05月16日 17:00",
        seat:"五号厅 5排14座 5排15座 ",
        count:2,
        status:2
      },
      {
        image:"http://jinglins.gitee.io/movie/img/movie2.b611b998.png",
        cinema:"万达国际影城",
        movieName:"帕丁顿熊",
        price:"60元",
        time:"05月16日 17:00",
        seat:"五号厅 5排14座 5排15座 ",
        count:1,
        status:3
      }
    ],
    filterLists:[] // 用于对数据筛选  页面展示数据为筛选的数据
  },
  filterList(){
    var newArr = JSON.parse(JSON.stringify(this.data.orderList));
    var filterArr = newArr.filter((ele) =>{
      if(this.data.currentTabIndex == 0){
        return true
      }else{
        return ele.status == this.data.currentTabIndex
      }
    })
    this.setData({
      filterLists:filterArr
    })
  },
  toggleTab(e){
    var index = e.currentTarget.dataset.index;
    this.setData({
      currentTabIndex:index
    })
    this.filterList();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      currentTabIndex:(options.id ? options.id : 0)
    })
    this.filterList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})